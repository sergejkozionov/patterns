package com.kozionov;

public interface Operation {

    void execute();
}
