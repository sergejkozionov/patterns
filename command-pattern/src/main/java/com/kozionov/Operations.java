package com.kozionov;

public class Operations {

    private double leftOperand;
    private double rightOperand;

    public Operations(double leftOperand, double rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    public void add() {
        System.out.println(leftOperand + rightOperand);
    }

    public void subtract() {
        System.out.println(leftOperand - rightOperand);
    }
}
