package com.kozionov;

public class OperationSubtract implements Operation {

    private final Operations operations;

    public OperationSubtract(Operations operations) {
        this.operations = operations;
    }

    @Override
    public void execute() {
        operations.subtract();
    }
}
