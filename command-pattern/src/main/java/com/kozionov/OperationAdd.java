package com.kozionov;

public class OperationAdd implements Operation {

    private final Operations operations;

    public OperationAdd(Operations operations) {
        this.operations = operations;
    }

    @Override
    public void execute() {
        operations.add();
    }
}
