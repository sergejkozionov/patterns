package com.kozionov;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private List<Operation> operations = new ArrayList<>();

    public void addOperation(Operation operation) {
        operations.add(operation);
    }

    public void performCalculations(){
        operations.forEach(Operation::execute);
    }

    public static void main(String[] args) {
        Operations operations = new Operations(5.0, 3.0);
        Operation operationAdd = new OperationAdd(operations);
        Operation operationSubtract = new OperationSubtract(operations);

        Calculator calculator = new Calculator();
        calculator.addOperation(operationAdd);
        calculator.addOperation(operationSubtract);
        calculator.performCalculations();
    }
}
