package com.kozionov;

public class Addition extends Operation {
    public Addition(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
