package com.kozionov;

public class VisitorImpl implements Visitor {
    @Override
    public void visit(Addition addition) {
        System.out.println(addition.getLeftOperand() + addition.getRightOperand());
    }

    @Override
    public void visit(Subtraction subtraction) {
        System.out.println(subtraction.getLeftOperand() - subtraction.getRightOperand());
    }
}
