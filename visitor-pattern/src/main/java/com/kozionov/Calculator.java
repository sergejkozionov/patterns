package com.kozionov;

public class Calculator {
    public static void main(String[] args) {
        double left = 5.0;
        double right = 3.0;
        Visitor visitor = new VisitorImpl();
        Operation addition = new Addition(left, right);
        Operation subtraction = new Subtraction(left, right);
        addition.accept(visitor);
        subtraction.accept(visitor);
    }
}
