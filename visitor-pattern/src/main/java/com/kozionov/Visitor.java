package com.kozionov;

public interface Visitor
{
    void visit(Addition addition);
    void visit(Subtraction subtraction);
}
