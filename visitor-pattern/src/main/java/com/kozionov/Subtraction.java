package com.kozionov;

public class Subtraction extends Operation {
    public Subtraction(double leftOperand, double rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
