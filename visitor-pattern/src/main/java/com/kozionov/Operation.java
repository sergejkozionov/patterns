package com.kozionov;

public abstract class Operation {

    private final double leftOperand;
    private final double rightOperand;

    public Operation(double leftOperand, double rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    public abstract void accept(Visitor visitor);

    public double getLeftOperand() {
        return leftOperand;
    }

    public double getRightOperand() {
        return rightOperand;
    }
}
