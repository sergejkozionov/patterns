package com.kozionov;

public class Addition implements OperationStrategy {

    @Override
    public double performOperation(double leftOperand, double rightOperand) {
        return leftOperand + rightOperand;
    }
}
