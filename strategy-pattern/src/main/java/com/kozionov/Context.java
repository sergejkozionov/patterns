package com.kozionov;

public class Context {
    private OperationStrategy strategy;

    public Context(OperationStrategy strategy) {
        this.strategy = strategy;
    }

    public double execute(double leftOperand, double rightOperand){
        return strategy.performOperation(leftOperand,rightOperand);
    }

    public static void main(String[] args) {
        double leftOperand = 5.0;
        double rightOperand = 3.0;
        Context additionContext = new Context(new Addition());
        Context subtractionContext = new Context(new Subtraction());
        System.out.println(additionContext.execute(leftOperand,rightOperand));
        System.out.println(subtractionContext.execute(leftOperand,rightOperand));
    }
}
