package com.kozionov;

public class Subtraction implements OperationStrategy {

    @Override
    public double performOperation(double leftOperand, double rightOperand) {
        return leftOperand - rightOperand;
    }
}
