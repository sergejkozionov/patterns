package com.kozionov;

public interface OperationStrategy {
        double performOperation(double leftOperand, double rightOperand);
}
