package com.kozionov.entity;

import com.kozionov.entity.id.Id;

public class Student extends Entity<Id> {

    private final String name;
    private final int age;
    private Student(Builder builder) {
        this.id(builder.id);
        this.name = builder.name;
        this.age = builder.age;
    }

    public static Builder newInstance() {
        return new Builder();
    }

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public static class Builder {

        private Id id;
        private String name;
        private int age;

        Builder() {
        }

        public Builder id(Id id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public void build() {
            new Student(this);
        }
    }
}
