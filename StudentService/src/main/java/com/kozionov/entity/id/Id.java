package com.kozionov.entity.id;

public class Id {
    private final String value;

    public Id(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
