package com.kozionov.entity;

import com.kozionov.entity.id.Id;

public abstract class Entity<T extends Id> {
    private T id;

    protected void id(T id) {
        this.id = id;
    }

    public T id() {
        return id;
    }
}
