package com.kozionov.dao;

import com.kozionov.entity.Entity;
import com.kozionov.entity.id.Id;

public interface StudentDao<I extends Id, E extends Entity<I>> extends Dao<I, E> {
}
