package com.kozionov.dao;

import com.kozionov.entity.Entity;
import com.kozionov.entity.id.Id;

import java.util.List;

public interface Dao<I extends Id, E extends Entity<I>> {
    void create(E entity);

    void update(E entity);

    void delete(I entityId);

    E get(I entityId);

    List<E> getAll();
}
