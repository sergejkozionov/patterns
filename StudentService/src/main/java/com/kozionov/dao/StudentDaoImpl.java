package com.kozionov.dao;

import com.kozionov.entity.Student;
import com.kozionov.entity.id.Id;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentDaoImpl implements StudentDao<Id, Student> {

    private final Map<Id, Student> storage = new HashMap<>();

    @Override
    public void create(Student entity) {
        storage.put(entity.id(), entity);
    }

    @Override
    public void update(Student entity) {
        storage.put(entity.id(), entity);

    }

    @Override
    public void delete(Id entityId) {
        storage.remove(entityId);
    }

    @Override
    public Student get(Id entityId) {
        return storage.get(entityId);
    }

    @Override
    public List<Student> getAll() {
        List<Student> students = new ArrayList<>();
        storage.forEach((id, student) -> students.add(student));
        return students;
    }
}
